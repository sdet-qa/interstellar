package com.interstellar.stringsearch;

import java.util.ArrayList;
import java.util.List;

/**
 * This is Java implementation of naive string searching algorithm
 * 
 * https://en.wikipedia.org/wiki/String-searching_algorithm#Naïve_string_search
 * https://www.geeksforgeeks.org/naive-algorithm-for-pattern-searching/
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class NaiveAlgorithm {

	/**
	 * Search pattern using naive algorithm
	 * 
	 * @return
	 */
	public List<Integer> searchAllOccurrences(String text, String pattern) {

		List<Integer> occurrences = new ArrayList<Integer>();

		int tLen = text.length();
		int pLen = pattern.length();

		for (int t = 0, p = 0; t < tLen - pLen + 1;) {

			int start = t;

			for (int _t = t; _t < t + pLen;) {

				if (text.charAt(_t) == pattern.charAt(p)) {

					_t++;
					p++;
				} else {

					p = 0;
					t = start + 1;

					break;
				}
			}

			if (p == pLen) {

				occurrences.add(start);

				t = start + 1;
				p = 0;
			}
		}

		return occurrences;
	}

	/**
	 * Driver method
	 * 
	 * @param args
	 */
	public static void main(String args[]) {

		String text = "abcdabcab";
		String pattern = "ab";

		NaiveAlgorithm na = new NaiveAlgorithm();

		List<Integer> occurrences = na.searchAllOccurrences(text, pattern);
		for (Integer occurrence : occurrences) {

			System.out.println("Match found at index: " + occurrence);
		}
	}
}
