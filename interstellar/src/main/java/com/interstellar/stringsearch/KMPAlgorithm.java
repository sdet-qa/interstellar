package com.interstellar.stringsearch;

import java.util.ArrayList;
import java.util.List;

/**
 * This is Java implementation of KMP string searching algorithm
 * 
 * https://en.wikipedia.org/wiki/Knuth–Morris–Pratt_algorithm
 * https://www.geeksforgeeks.org/kmp-algorithm-for-pattern-searching/
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class KMPAlgorithm {

	/**
	 * Compute longest prefix suffix array
	 * 
	 * @param pattern
	 * @return
	 */
	private int[] computePsa(String pattern) {

		int[] psa = new int[pattern.length()];
		psa[0] = 0;

		for (int j = 0, i = 1; i < psa.length;) {

			if (pattern.charAt(i) == pattern.charAt(j)) {

				psa[i] = j + 1;

				i++;
				j++;
			} else {

				if (j == 0) {

					psa[i++] = 0;
				} else {

					j = psa[j - 1];
				}
			}
		}

		return psa;
	}

	/**
	 * Search pattern using KMP algorithm
	 * 
	 * @param text
	 * @param pattern
	 */
	public List<Integer> searchAllOccurrences(String text, String pattern) {

		int[] psa = computePsa(pattern);

		List<Integer> occurrences = new ArrayList<Integer>();

		for (int t = 0, p = 0; t < text.length() && p < pattern.length();) {

			if (text.charAt(t) == pattern.charAt(p)) {

				t++;
				p++;
			} else {

				if (p == 0) {

					t++;
				} else {

					p = psa[p - 1];
				}
			}

			if (p == pattern.length()) {

				occurrences.add(t - p);

				p = psa[p - 1];
			}
		}

		return occurrences;
	}

	/**
	 * Driver method
	 * 
	 * @param args
	 */
	public static void main(String args[]) {

		String text = "aaaa";
		String pattern = "aa";

		KMPAlgorithm kmpAll = new KMPAlgorithm();

		List<Integer> occurrences = kmpAll.searchAllOccurrences(text, pattern);
		for (Integer occurrence : occurrences) {

			System.out.println("Match found at index: " + occurrence);
		}
	}
}