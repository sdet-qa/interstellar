package com.interstellar.stringsearch;

import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class KMPAlgorithmTest {

	/**
	 * Data provider
	 * 
	 * @return
	 */
	@DataProvider(name = "kmpData")
	public Object[][] getData() {

		List<Integer> l1 = Arrays.asList(0, 1, 2);
		List<Integer> l2 = Arrays.asList(10);
		List<Integer> l3 = Arrays.asList();

		return new Object[][] { { "aaaa", "aa", l1 }, { "ABABDABACDABABCABAB", "ABABCABAB", l2 },
				{ "Hello there!", "No Match", l3 } };
	}

	/**
	 * Test method for string search using KMP
	 * 
	 * @param text
	 * @param pattern
	 * @param expectedList
	 */
	@Test(dataProvider = "kmpData")
	public void validateKMPSearchTest(String text, String pattern, List<Integer> expectedList) {

		KMPAlgorithm kmpAlgo = new KMPAlgorithm();
		List<Integer> actualList = kmpAlgo.searchAllOccurrences(text, pattern);

		Assert.assertTrue(expectedList.equals(actualList), "Expected list is not equal to actual list");
	}
}
