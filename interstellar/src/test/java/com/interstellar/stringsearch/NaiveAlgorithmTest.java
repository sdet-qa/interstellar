package com.interstellar.stringsearch;

import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * 
 * @author Mirzafazulur Rahamanbaig
 *
 */
public class NaiveAlgorithmTest {

	/**
	 * Data provider
	 * 
	 * @return
	 */
	@DataProvider(name = "strData")
	public Object[][] getData() {

		List<Integer> l1 = Arrays.asList(0, 1, 2);
		List<Integer> l2 = Arrays.asList(10);
		List<Integer> l3 = Arrays.asList();

		return new Object[][] { { "aaaa", "aa", l1 }, { "ABABDABACDABABCABAB", "ABABCABAB", l2 },
				{ "Hello there!", "No Match", l3 } };
	}

	/**
	 * Test method for string search using naive algorithm
	 * 
	 * @param text
	 * @param pattern
	 * @param expectedList
	 */
	@Test(dataProvider = "strData")
	public void validateNaiveSearchTest(String text, String pattern, List<Integer> expectedList) {

		NaiveAlgorithm na = new NaiveAlgorithm();
		List<Integer> actualList = na.searchAllOccurrences(text, pattern);

		Assert.assertTrue(expectedList.equals(actualList), "Expected list is not equal to actual list");
	}
}
